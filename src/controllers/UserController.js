const User = require('../models/UserM');

module.exports = {
    store,
    index
}

async function store (req, res) {
    const { name, email } = req.body;

    const user = await User.create({ name, email }).then(response => {
        return res.status(200).json(response.get());

    }).catch(err => {
        return res.status(500).json({"error": err});
    });
}

async function index(req, res) {
    const users = await User.findAll()
    
    // console.log(users)
    return res.json(users)
}
